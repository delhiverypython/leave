import urllib.request
import json
import urllib

data = {    
    "super_id":"20",
    "leave_type":"Annual leave",
    "start_date":"2019-08-06 00:00:00",
    "end_date":"2019-08-08 00:00:00",
    "comment":"Annual leave"

        }
myurl = "http://127.0.0.1:5000/2/leaves"
req = urllib.request.Request(myurl)
req.add_header('Content-Type','application/json; charset=utf-8')
jsondata = json.dumps(data)
jsondataasbytes = jsondata.encode('utf-8')
req.add_header('Content-Length', len(jsondataasbytes))
print(jsondataasbytes)
response = urllib.request.urlopen(req,jsondataasbytes)
print(response.read().decode('utf-8'))