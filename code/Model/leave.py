from db import db
import datetime
from marshmallow import Schema, fields, post_load, validates, ValidationError, validates_schema

class Leave(db.Model):
    l_id = db.Column(db.Integer, primary_key = True, unique = True, nullable = True)
    id = db.Column(db.Integer, nullable = False)
    super_id = db.Column(db.Integer, nullable = False)
    title = db.Column(db.Text, nullable = False)
    leave_type = db.Column(db.Text, nullable = False)
    start_date = db.Column(db.DateTime, nullable = False)
    end_date = db.Column(db.DateTime, nullable = False)
    comment = db.Column(db.Text, nullable = False)
    status = db.Column(db.Text, default = 'Pending')

class LeaveSchema(Schema):
    l_id = fields.Integer()
    id = fields.Integer()
    super_id = fields.Integer()
    leave_type = fields.String()
    title = fields.String()
    start_date = fields.Date()
    end_date = fields.Date()
    comment = fields.String()
    status = fields.String()

    @validates_schema
    def validate_dates(self, data, **kwargs):
        if data["start_date"] > data["end_date"]:
            raise ValidationError("End date must be after start date")


    @post_load
    def add_leave(self, data):
        return Leave(**data)

    @validates('start_date')
    def validate_start_date(self, cdate):
        todays = datetime.date.today()
        if todays > cdate:
            raise ValidationError('Previous date not allowed')

   
    