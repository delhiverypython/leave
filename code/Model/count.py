from db import db
import datetime
from marshmallow import Schema, fields, post_load, validates, ValidationError, validates_schema

class Count(db.Model):

    id = db.Column(db.Integer,primary_key = True, unique = True, nullable = False)
    m_count = db.Column(db.Integer,default = 5)
    Annual_count = db.Column(db.Integer, default = 32)
    p_count = db.Column(db.Integer, default = 5)
   
class CountSchema(Schema):

    id = fields.Integer()
    m_count = fields.Integer()
    Annual_count = fields.Integer()
    p_count = fields.Integer()
    

    

   
    