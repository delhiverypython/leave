from Model.leave import Leave, LeaveSchema
from flask import request, jsonify
from db import db
from flask_restplus import Resource

class Action_leave(Resource):
    def get(self, id):
        rows = Leave.query.filter_by(super_id = id,status = "Pending").all()
        if rows:
            schema = LeaveSchema(many = True,exclude=('id','super_id'))
            leave = schema.dump(rows)
            if leave[1]:
                return leave[1]
            return jsonify(leave.data)
        else:
            return {'message':'No pending request found'}

    def put(self,id):
        data = request.get_json()
        l_id = data['l_id']
        row = Leave.query.filter_by(l_id = l_id).first() 
        try:
            row.status = data['status']
            db.session.commit()
        except:
            return jsonify({'alert':'Some error occured'})
        
        return jsonify({'message':'Your response is noted'})