from Model.count import Count, CountSchema
from flask import request, jsonify
from db import db
from flask_restplus import Resource

class Counts(Resource):
    def get(self, id):
        rows = Count.query.filter_by(id = id).all()
        schema = CountSchema(many = True)
        count = schema.dump(rows).data
        return jsonify(count)