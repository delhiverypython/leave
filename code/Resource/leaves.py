from Model.leave import Leave, LeaveSchema
from flask import request, jsonify
from db import db
from flask_restplus import Resource

class Leaves(Resource):
    def get(self, id):
        rows = Leave.query.filter_by(id = id).all()
        if rows:
            schema = LeaveSchema(many = True, exclude=('id','super_id'))
            leave = schema.dump(rows)
            if leave[1]:
                return leave[1]
            return jsonify(leave.data)    
        else:
            return {'message: No entry found'}
        

    def post(self, id):
        data = request.get_json()
        data['id'] = id
        schema = LeaveSchema()
        leave = schema.load(data)
        try:
            db.session.add(leave.data)
            db.session.commit()
        except:
            return leave[1]

        return jsonify({'message':'data added successfully'})
            
    def delete(self,id):
        data = request.get_json()
        l_id = data['l_id']
        row = Leave.query.filter_by(l_id = l_id).first()
        try:
            db.session.delete(row)
            db.session.commit()
        except:
            return jsonify({'Alert':'Error or No request is present'})

        return jsonify({'message':'Response deleted'})   