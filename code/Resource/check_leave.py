from Model.leave import Leave, LeaveSchema
from flask import request, jsonify
from db import db
from flask_restplus import Resource

class Check_leave(Resource):
    def get(self,id):
        title = request.args.get('title')
        leave_type = request.args.get('leave_type')
        if title and leave_type:
            rows = Leave.query.filter_by(title = title,id = id,leave_type = leave_type).all()
        elif title:
            rows = Leave.query.filter_by(title = title,id = id).all()
        elif leave_type:
            rows = Leave.query.filter_by(id = id,leave_type = leave_type).all()
        else:
            rows = Leave.query.filter_by(id = id).all()

        if rows:
            schema = LeaveSchema(many = True,exclude=('id','super_id'))
            leave = schema.dump(rows)
            if leave[1]:
                return leave[1]
            return jsonify(leave.data)
        else:
            return {'message':'No entry found'}