from flask import Flask
from flask_restplus import Api
from Resource.leaves import Leaves
from Resource.check_leave import Check_leave
from Resource.leave_action import Action_leave
from Resource.counts import Counts
from db import db

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql:///Leave'
app.config['SQLALCHEMY_TRACK-MODIFICATIONS'] = True
app.config['SECRET_KEY'] = "leave"
api = Api(app)

api.add_resource(Leaves,'/<int:id>/leaves')
api.add_resource(Check_leave,'/<int:id>/leaves/')
api.add_resource(Action_leave,'/<int:id>/leaves/action')

api.add_resource(Counts,'/<int:id>/counts')

@app.before_first_request
def create_table():
    db.create_all()

if __name__ == "__main__":
    db.init_app(app)
    app.run(debug=True)